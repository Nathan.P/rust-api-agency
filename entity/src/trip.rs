use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Eq, DeriveEntityModel, Serialize, Deserialize)]
#[sea_orm(table_name = "trips")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: i32,
    pub name: String,
    pub max_reservation: i32,
    pub price: i32,
    pub start_date: Date,
    pub end_date: Date,
    pub seller_id: i32
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "super::seller::Entity",
        from = "Column::SellerId",
        to = "super::seller::Column::Id"
    )]
    Seller,
}

impl Related<super::seller::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Seller.def()
    }
}

impl Related<super::client::Entity> for Entity {
    fn to() -> RelationDef {
        super::reservation::Relation::Client.def()
    }

    fn via() -> Option<RelationDef> {
        Some(super::reservation::Relation::Trip.def().rev())
    }
}

impl ActiveModelBehavior for ActiveModel {}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct CreateModel {
    pub name: String,
    pub max_reservation: i32,
    pub price: i32,
    pub start_date: Date,
    pub end_date: Date,
    pub seller_id: i32
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct UpdateModel {
    pub name: String,
    pub max_reservation: i32,
    pub price: i32,
    pub start_date: Date,
    pub end_date: Date,
    pub seller_id: i32
}