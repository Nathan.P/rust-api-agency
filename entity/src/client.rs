use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Eq, DeriveEntityModel, Serialize, Deserialize)]
#[sea_orm(table_name = "clients")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: i32,
    pub firstname: String,
    pub lastname: String,
    pub email: String,
    pub phone_number: String,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {}

impl Related<super::reservation::Entity> for Entity {
    fn to() -> RelationDef {
        super::reservation::Relation::Trip.def()
    }

    fn via() -> Option<RelationDef> {
        Some(super::reservation::Relation::Client.def().rev())
    }
}

impl ActiveModelBehavior for ActiveModel {}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct CreateModel {
    pub firstname: String,
    pub lastname: String,
    pub email: String,
    pub phone_number: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct UpdateModel {
    pub firstname: String,
    pub lastname: String,
    pub email: String,
    pub phone_number: String,
}