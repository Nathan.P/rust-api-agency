pub use sea_orm_migration::prelude::*;

mod m20221128_101224_clients;
mod m20221128_101447_trips;
mod m20221128_101458_reservations;
mod m20221128_160741_sellers;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20221128_101224_clients::Migration),
            Box::new(m20221128_101447_trips::Migration),
            Box::new(m20221128_101458_reservations::Migration),
            Box::new(m20221128_160741_sellers::Migration),
        ]
    }
}
