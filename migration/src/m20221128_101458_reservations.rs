use entity::reservation::*;
use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                sea_query::Table::create()
                    .table(Entity)
                    .if_not_exists()
                    .col(ColumnDef::new(Column::TripId).integer().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-reservation-permission-id")
                            .from(Entity, Column::TripId)
                            .to(entity::trip::Entity, entity::trip::Column::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .col(ColumnDef::new(Column::ClientId).integer().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-reservation-client-id")
                            .from(Entity, Column::ClientId)
                            .to(entity::client::Entity, entity::client::Column::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .primary_key(
                        Index::create()
                            .name("pk-reservation")
                            .col(Column::TripId)
                            .col(Column::ClientId)
                            .primary(),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(sea_query::Table::drop().table(Entity).to_owned())
            .await
    }
}
