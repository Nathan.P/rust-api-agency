use crate::services::trip;
use crate::AppState;
use actix_web::{delete, get, post, put, web, HttpResponse, Responder};
use entity::trip::{CreateModel, UpdateModel};

#[get("/trips")]
async fn find_all(app_state: web::Data<AppState>) -> impl Responder {
    match trip::find_all(&app_state.connection).await {
        Ok(trips) => HttpResponse::Ok().json(trips),
        Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
    }
}

#[get("/trips/{id}")]
async fn find(app_state: web::Data<AppState>, id: web::Path<String>) -> impl Responder {
    match id.parse::<i32>() {
        Ok(id) => match trip::find_one(id, &app_state.connection).await {
            Ok(trip_option) => match trip_option {
                Some(trip) => HttpResponse::Ok().json(trip),
                None => HttpResponse::NotFound().body("Trip not found"),
            },
            Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
        },
        Err(err) => HttpResponse::NotFound().body(err.to_string()),
    }
}

#[post("/trips")]
async fn create(app_state: web::Data<AppState>, trip: web::Json<CreateModel>) -> impl Responder {
    match trip::create(trip.to_owned(), &app_state.connection).await {
        Ok(trip) => HttpResponse::Ok().json(trip),
        Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
    }
}

#[put("/trips/{id}")]
async fn update(
    app_state: web::Data<AppState>,
    id: web::Path<String>,
    trip: web::Json<UpdateModel>,
) -> impl Responder {
    match id.parse::<i32>() {
        Ok(id) => {
            match trip::update(
                id,
                trip.into_inner(),
                &app_state.connection,
            )
            .await
            {
                Ok(trip_option) => match trip_option {
                    Some(trip) => HttpResponse::Ok().json(trip),
                    None => HttpResponse::NotFound().body("Trip not found"),
                },
                Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
            }
        }
        Err(err) => HttpResponse::NotFound().body(err.to_string()),
    }
}

#[delete("/trips/{id}")]
async fn delete(app_state: web::Data<AppState>, id: web::Path<String>) -> impl Responder {
    match id.parse::<i32>() {
        Ok(id) => match trip::delete(id, &app_state.connection).await {
            Ok(delete_result) => match delete_result {
                Some(result) => {
                    HttpResponse::Ok().body(format!("{} row deleted", result.rows_affected))
                }
                None => HttpResponse::Ok().body("No trip deleted"),
            },
            Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
        },
        Err(err) => HttpResponse::NotFound().body(err.to_string()),
    }
}

pub fn init_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(find_all);
    cfg.service(find);
    cfg.service(create);
    cfg.service(update);
    cfg.service(delete);
}
