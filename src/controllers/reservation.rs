use actix_web::{delete, post, web, HttpResponse, Responder};

use crate::{AppState, services::reservation};

#[post("/reservations/{user_id}/{permission_id}")]
async fn create(app_state: web::Data<AppState>, path: web::Path<(i32, i32)>) -> impl Responder {
    let (trip_id, client_id) = path.into_inner();

    match reservation::create(trip_id, client_id, &app_state.connection).await {
        Ok(user_permission) => HttpResponse::Ok().json(user_permission),
        Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
    }
}

#[delete("/reservations/{user_id}/{permission_id}")]
async fn delete(app_state: web::Data<AppState>, path: web::Path<(i32, i32)>) -> impl Responder {
    let (trip_id, client_id) = path.into_inner();

    match reservation::delete(trip_id, client_id, &app_state.connection).await {
        Ok(user_permission) => match user_permission {
            Some(result) => {
                HttpResponse::Ok().body(format!("{} row deleted", result.rows_affected))
            }
            None => HttpResponse::Ok().body("No permission deleted"),
        },
        Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
    }
}

pub fn init_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(create);
    cfg.service(delete);
}
