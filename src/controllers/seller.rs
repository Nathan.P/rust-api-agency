use crate::services::seller;
use crate::AppState;
use actix_web::{delete, get, post, put, web, HttpResponse, Responder};
use entity::seller::{CreateModel, UpdateModel};

#[get("/sellers")]
async fn find_all(app_state: web::Data<AppState>) -> impl Responder {
    match seller::find_all(&app_state.connection).await {
        Ok(sellers) => HttpResponse::Ok().json(sellers),
        Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
    }
}

#[get("/sellers/{id}")]
async fn find(app_state: web::Data<AppState>, id: web::Path<String>) -> impl Responder {
    match id.parse::<i32>() {
        Ok(id) => match seller::find_one(id, &app_state.connection).await {
            Ok(seller_option) => match seller_option {
                Some(seller) => HttpResponse::Ok().json(seller),
                None => HttpResponse::NotFound().body("Seller not found"),
            },
            Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
        },
        Err(err) => HttpResponse::NotFound().body(err.to_string()),
    }
}

#[post("/sellers")]
async fn create(app_state: web::Data<AppState>, seller: web::Json<CreateModel>) -> impl Responder {
    match seller::create(seller.to_owned(), &app_state.connection).await {
        Ok(seller) => HttpResponse::Ok().json(seller),
        Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
    }
}

#[put("/sellers/{id}")]
async fn update(
    app_state: web::Data<AppState>,
    id: web::Path<String>,
    seller: web::Json<UpdateModel>,
) -> impl Responder {
    match id.parse::<i32>() {
        Ok(id) => {
            match seller::update(
                id,
                seller.into_inner(),
                &app_state.connection,
            )
            .await
            {
                Ok(seller_option) => match seller_option {
                    Some(seller) => HttpResponse::Ok().json(seller),
                    None => HttpResponse::NotFound().body("Seller not found"),
                },
                Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
            }
        }
        Err(err) => HttpResponse::NotFound().body(err.to_string()),
    }
}

#[delete("/sellers/{id}")]
async fn delete(app_state: web::Data<AppState>, id: web::Path<String>) -> impl Responder {
    match id.parse::<i32>() {
        Ok(id) => match seller::delete(id, &app_state.connection).await {
            Ok(delete_result) => match delete_result {
                Some(result) => {
                    HttpResponse::Ok().body(format!("{} row deleted", result.rows_affected))
                }
                None => HttpResponse::Ok().body("No seller deleted"),
            },
            Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
        },
        Err(err) => HttpResponse::NotFound().body(err.to_string()),
    }
}

pub fn init_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(find_all);
    cfg.service(find);
    cfg.service(create);
    cfg.service(update);
    cfg.service(delete);
}
