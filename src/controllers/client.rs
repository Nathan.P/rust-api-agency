use crate::services::client;
use crate::AppState;
use actix_web::{delete, get, post, put, web, HttpResponse, Responder};
use entity::client::{CreateModel, UpdateModel};

#[get("/clients")]
async fn find_all(app_state: web::Data<AppState>) -> impl Responder {
    match client::find_all(&app_state.connection).await {
        Ok(clients) => HttpResponse::Ok().json(clients),
        Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
    }
}

#[get("/clients/{id}")]
async fn find(app_state: web::Data<AppState>, id: web::Path<String>) -> impl Responder {
    match id.parse::<i32>() {
        Ok(id) => match client::find_one(id, &app_state.connection).await {
            Ok(client_option) => match client_option {
                Some(client) => HttpResponse::Ok().json(client),
                None => HttpResponse::NotFound().body("Client not found"),
            },
            Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
        },
        Err(err) => HttpResponse::NotFound().body(err.to_string()),
    }
}

#[post("/clients")]
async fn create(app_state: web::Data<AppState>, client: web::Json<CreateModel>) -> impl Responder {
    match client::create(client.to_owned(), &app_state.connection).await {
        Ok(client) => HttpResponse::Ok().json(client),
        Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
    }
}

#[put("/clients/{id}")]
async fn update(
    app_state: web::Data<AppState>,
    id: web::Path<String>,
    client: web::Json<UpdateModel>,
) -> impl Responder {
    match id.parse::<i32>() {
        Ok(id) => {
            match client::update(
                id,
                client.into_inner(),
                &app_state.connection,
            )
            .await
            {
                Ok(client_option) => match client_option {
                    Some(client) => HttpResponse::Ok().json(client),
                    None => HttpResponse::NotFound().body("Client not found"),
                },
                Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
            }
        }
        Err(err) => HttpResponse::NotFound().body(err.to_string()),
    }
}

#[delete("/clients/{id}")]
async fn delete(app_state: web::Data<AppState>, id: web::Path<String>) -> impl Responder {
    match id.parse::<i32>() {
        Ok(id) => match client::delete(id, &app_state.connection).await {
            Ok(delete_result) => match delete_result {
                Some(result) => {
                    HttpResponse::Ok().body(format!("{} row deleted", result.rows_affected))
                }
                None => HttpResponse::Ok().body("No client deleted"),
            },
            Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
        },
        Err(err) => HttpResponse::NotFound().body(err.to_string()),
    }
}

pub fn init_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(find_all);
    cfg.service(find);
    cfg.service(create);
    cfg.service(update);
    cfg.service(delete);
}
