use actix_web::{web, App, HttpServer};
use migration::{Migrator, MigratorTrait};
use sea_orm::{Database, DatabaseConnection};

mod controllers;
mod services;

#[derive(Debug, Clone)]
struct AppState {
    connection: DatabaseConnection,
}

#[actix_web::main]
pub async fn main() -> std::io::Result<()> {
    let connection: DatabaseConnection = Database::connect("sqlite::memory:").await.unwrap();
    Migrator::up(&connection, None).await.unwrap();

    let state = AppState { connection };

    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(state.clone()))
            .configure(controllers::client::init_routes)
            .configure(controllers::trip::init_routes)
            .configure(controllers::seller::init_routes)
            .configure(controllers::reservation::init_routes)
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
