use entity::trip::{CreateModel, Entity, Model, UpdateModel};
use sea_orm::{
    entity::ActiveValue, ActiveModelTrait, DatabaseConnection, DbErr, DeleteResult, EntityTrait,
    IntoActiveModel, ModelTrait,
};

pub async fn find_one(id: i32, conn: &DatabaseConnection) -> Result<Option<Model>, DbErr> {
    Entity::find_by_id(id).one(conn).await
}

pub async fn find_all(conn: &DatabaseConnection) -> Result<Vec<Model>, DbErr> {
    Entity::find().all(conn).await
}

pub async fn create(trip: CreateModel, conn: &DatabaseConnection) -> Result<Model, DbErr> {
    entity::trip::ActiveModel {
        name: ActiveValue::Set(trip.name),
        max_reservation: ActiveValue::Set(trip.max_reservation),
        price: ActiveValue::Set(trip.max_reservation),
        start_date: ActiveValue::Set(trip.start_date),
        end_date: ActiveValue::Set(trip.end_date),
        seller_id: ActiveValue::Set(trip.seller_id),
        ..Default::default()
    }
    .insert(conn)
    .await
}

pub async fn update(
    id: i32,
    trip: UpdateModel,
    conn: &DatabaseConnection,
) -> Result<Option<Model>, DbErr> {
    match find_one(id, conn).await? {
        Some(trip_in_db) => {
            if trip.name != trip_in_db.name
                || trip.max_reservation != trip_in_db.max_reservation
                || trip.price != trip_in_db.price
                || trip.start_date != trip_in_db.start_date
                || trip.end_date != trip_in_db.end_date
                || trip.seller_id != trip_in_db.seller_id
            {
                let mut active_model = trip_in_db.clone().into_active_model();
                
                if trip.name != trip_in_db.name {
                    active_model.name = ActiveValue::Set(trip.name.to_owned());
                }
                
                if trip.max_reservation != trip_in_db.max_reservation {
                    active_model.name = ActiveValue::Set(trip.name.to_owned());
                }
                
                if trip.price != trip_in_db.price {
                    active_model.name = ActiveValue::Set(trip.name.to_owned());
                }
                
                if trip.start_date != trip_in_db.start_date {
                    active_model.name = ActiveValue::Set(trip.name.to_owned());
                }
                
                if trip.end_date != trip_in_db.end_date {
                    active_model.name = ActiveValue::Set(trip.name.to_owned());
                }
                
                if trip.seller_id != trip_in_db.seller_id {
                    active_model.name = ActiveValue::Set(trip.name.to_owned());
                }

                Ok(Some(active_model.update(conn).await?))
            } else {
                Ok(None)
            }
        }
        None => Ok(None),
    }
}

pub async fn delete(id: i32, conn: &DatabaseConnection) -> Result<Option<DeleteResult>, DbErr> {
    match Entity::find_by_id(id).one(conn).await? {
        Some(entity) => Ok(Some(entity.delete(conn).await?)),
        None => Ok(None),
    }
}
