use entity::client::{CreateModel, Entity, Model, UpdateModel};
use sea_orm::{
    entity::ActiveValue, ActiveModelTrait, DatabaseConnection, DbErr, DeleteResult, EntityTrait,
    IntoActiveModel, ModelTrait,
};

pub async fn find_one(id: i32, conn: &DatabaseConnection) -> Result<Option<Model>, DbErr> {
    Entity::find_by_id(id).one(conn).await
}

pub async fn find_all(conn: &DatabaseConnection) -> Result<Vec<Model>, DbErr> {
    Entity::find().all(conn).await
}

pub async fn create(client: CreateModel, conn: &DatabaseConnection) -> Result<Model, DbErr> {
    entity::client::ActiveModel {
        firstname: ActiveValue::Set(client.firstname),
        lastname: ActiveValue::Set(client.lastname),
        email: ActiveValue::Set(client.email),
        phone_number: ActiveValue::Set(client.phone_number),
        ..Default::default()
    }
    .insert(conn)
    .await
}

pub async fn update(
    id: i32,
    client: UpdateModel,
    conn: &DatabaseConnection,
) -> Result<Option<Model>, DbErr> {
    match find_one(id, conn).await? {
        Some(client_in_db) => {
            if client.firstname != client.firstname
                || client.lastname != client_in_db.lastname
                || client.email != client_in_db.email
                || client.phone_number != client_in_db.phone_number
            {
                let mut active_model = client_in_db.clone().into_active_model();

                if client.firstname != client_in_db.firstname {
                    active_model.firstname = ActiveValue::Set(client.firstname.to_owned());
                }

                if client.lastname != client_in_db.lastname {
                    active_model.lastname = ActiveValue::Set(client.lastname.to_owned());
                }

                if client.email != client_in_db.email {
                    active_model.email = ActiveValue::Set(client.email.to_owned());
                }

                if client.phone_number != client_in_db.phone_number {
                    active_model.phone_number = ActiveValue::Set(client.phone_number.to_owned());
                }

                Ok(Some(active_model.update(conn).await?))
            } else {
                Ok(None)
            }
        }
        None => Ok(None),
    }
}

pub async fn delete(id: i32, conn: &DatabaseConnection) -> Result<Option<DeleteResult>, DbErr> {
    match Entity::find_by_id(id).one(conn).await? {
        Some(entity) => Ok(Some(entity.delete(conn).await?)),
        None => Ok(None),
    }
}
