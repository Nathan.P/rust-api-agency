use entity::reservation::{Entity, Model};
use sea_orm::{
    entity::ActiveValue, ActiveModelTrait, DatabaseConnection, DbErr, DeleteResult, EntityTrait,
    ModelTrait,
};

pub async fn create(
    trip_id: i32,
    client_id: i32,
    conn: &DatabaseConnection,
) -> Result<Model, DbErr> {
    entity::reservation::ActiveModel {
        trip_id: ActiveValue::Set(trip_id),
        client_id: ActiveValue::Set(client_id),
        ..Default::default()
    }
    .insert(conn)
    .await
}

pub async fn delete(
    trip_id: i32,
    client_id: i32,
    conn: &DatabaseConnection,
) -> Result<Option<DeleteResult>, DbErr> {
    match Entity::find_by_id((trip_id, client_id)).one(conn).await? {
        Some(entity) => Ok(Some(entity.delete(conn).await?)),
        None => Ok(None),
    }
}
