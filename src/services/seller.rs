use entity::seller::{CreateModel, Entity, Model, UpdateModel};
use sea_orm::{
    entity::ActiveValue, ActiveModelTrait, DatabaseConnection, DbErr, DeleteResult, EntityTrait,
    IntoActiveModel, ModelTrait,
};

pub async fn find_one(id: i32, conn: &DatabaseConnection) -> Result<Option<Model>, DbErr> {
    Entity::find_by_id(id).one(conn).await
}

pub async fn find_all(conn: &DatabaseConnection) -> Result<Vec<Model>, DbErr> {
    Entity::find().all(conn).await
}

pub async fn create(seller: CreateModel, conn: &DatabaseConnection) -> Result<Model, DbErr> {
    entity::seller::ActiveModel {
        name: ActiveValue::Set(seller.name),
        ..Default::default()
    }
    .insert(conn)
    .await
}

pub async fn update(
    id: i32,
    seller: UpdateModel,
    conn: &DatabaseConnection,
) -> Result<Option<Model>, DbErr> {
    match find_one(id, conn).await? {
        Some(seller_in_db) => {
            if seller.name != seller.name {
                let mut active_model = seller_in_db.clone().into_active_model();

                active_model.name = ActiveValue::Set(seller.name.to_owned());

                Ok(Some(active_model.update(conn).await?))
            } else {
                Ok(None)
            }
        }
        None => Ok(None),
    }
}

pub async fn delete(id: i32, conn: &DatabaseConnection) -> Result<Option<DeleteResult>, DbErr> {
    match Entity::find_by_id(id).one(conn).await? {
        Some(entity) => Ok(Some(entity.delete(conn).await?)),
        None => Ok(None),
    }
}
